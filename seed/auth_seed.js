#!/usr/bin/env node

var path = require('path')
var async = require('async')
var yaktor = require('yaktor')

function readDataFile(fn){
  // var data = JSON.parse(require("fs").readFileSync(path.resolve('seed','data',fn)).toString());
  console.log("Reading file: ", fn);
  var data = require(path.resolve("seed", "data", fn));
  return data
}

function rxify (str) {
  return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\^\$\|\\]/g, '\\$&')
}

process.on('uncaughtException', function (err) {
  console.log(err.stack)
})

/* BEGIN AUTH SEED DATA */
var accessClients = readDataFile('accessClients.json')
var roles = readDataFile('roles.json')
var users = readDataFile('users.json')
/* END AUTH SEED DATA */

async.eachSeries([ '02_mongo', '02_shortid', '03_schema' ], function (it, next) {
  var config = require(path.resolve('config', 'global', it))
  if (typeof config === 'function') {
    config(yaktor, next)
  } else {
    next()
  }
}, function (err) {
  if (err) {
    console.log('ERROR!', err, err.stack)
    return process.exit(1)
  }

  var converter = require(path.join('yaktor', 'services', 'conversionService'))
  var mongoose = require('mongoose')
  var Role = mongoose.model('Role')
  var User = mongoose.model('User')
  var AccessClient = mongoose.model('AccessClient')

  var seed = function (done) {
    async.series([
      // remove
      function (next) {
        async.parallel([ function (next) {
          AccessClient.remove({ _id: { $in: accessClients.map(function (ac) { return ac._id }) } }, next)
        }, function (next) {
          Role.remove({ path: new RegExp('[' + roles.map(function (r) { return rxify(r.path) }).join('|') + ']') }, next)
        }, function (next) {
          User.remove({ _id: { $in: users.map(function (u) { return u.email }) } }, next)
        } ], next)
      },
      // add
      function (next) {
        async.series([
          function (next) {
            async.each(accessClients, function (accessClient, next) {
              new AccessClient(accessClient).save(next)
            }, next)
          },
          function (next) {
            async.each(roles, function (role, next) {
              converter.fromDto('OAuth2.Role', role, function (err, role) {
                if (err) {
                  return next(err)
                }
                role.save(next)
              })
            }, next)
          },
          function (next) {
            async.each(users,
              function (user, next) {
                converter.fromDto('StashMyBag.User', user, function (err, User) {
                  if (err) {
                    return next(err)
                  }
                  User.save(next)
                })
              }, next)
          }
        ], next)
      }
    ], done)
  }

  seed(function (err) {
    if (err) console.log(err)
    process.exit(err ? 1 : 0)
  })
})
