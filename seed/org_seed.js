#!/usr/bin/env node

var path = require('path')
var async = require('async')
var yaktor = require('yaktor')

function readDataFile(fn){
  // var data = JSON.parse(require("fs").readFileSync(path.resolve('seed','data',fn)).toString());
  console.log("Reading file: ", fn);
  var data = require(path.resolve("seed", "data", fn));
  return data
}

function rxify (str) {
  return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\^\$\|\\]/g, '\\$&')
}

process.on('uncaughtException', function (err) {
  console.log(err.stack)
})

/* BEGIN SEED DATA */
var organizations = readDataFile('organizations.json')
var admins = readDataFile('Admins.json')
var tags = readDataFile('tags.json')
var stashes = readDataFile('stashs.json')//don't get hung up spelling, just letting it follow the known pattern
/* END SEED DATA */

async.eachSeries([ '02_mongo', '02_shortid', '03_schema' ], function (it, next) {
  var config = require(path.resolve('config', 'global', it))
  if (typeof config === 'function') {
    config(yaktor, next)
  } else {
    next()
  }
}, function (err) {
  if (err) {
    console.log('ERROR!', err, err.stack)
    return process.exit(1)
  }

  var converter = require(path.join('yaktor', 'services', 'conversionService'))
  var mongoose = require('mongoose')
  var Organization = mongoose.model('Organization')
  var Tag = mongoose.model('Tag')
  var Stash = mongoose.model('Stash')
  var OrganizationUser = mongoose.model('OrganizationUser')
  var Admin = mongoose.model('Admin')

  var seed = function (done) {
    async.series([
      // remove
      function (next) {
        async.parallel([ function (next) {
          Organization.remove({ _id: { $in: organizations.map(function (ac) { return ac._id }) } }, next)
        }, function (next) {
          Tag.remove({ _id: { $in: tags.map(function (ac) { return ac._id }) } }, next)
        }, function (next) {
          Stash.remove({ _id: { $in: stashes.map(function (ac) { return ac._id }) } }, next)
        }, function (next) {
          OrganizationUser.remove({ _id: { $in: admins.map(function (ac) { return ac._id }) } }, next)
        }], next)
      },
      // add
      function (next) {
        async.series([
          function (next) {
            async.each(organizations, function (organization, next) {
              new Organization(organization).save(next)
            }, next)
          },
          function (next) {
            async.each(admins, function (admin, next) {
              new Admin(admin).save(next)
            }, next)
          },
          function (next) {
            async.each(stashes, function (stash, next) {
              converter.fromDto('StashMyBag.Stash', stash, function (err, stash) {
                if (err) {
                  return next(err)
                }
                stash.save(next)
              })
            }, next)
          },
          function (next) {
            async.each(tags, function (tag, next) {
              converter.fromDto('StashMyBag.Tag', tag, function (err, tag) {
                if (err) {
                  return next(err)
                }
                tag.save(next)
              })
            }, next)
          }
        ], next)
      }
    ], done)
  }

  seed(function (err) {
    if (err) console.log(err)
    process.exit(err ? 1 : 0)
  })
})
